# CKAD Study Track practice questions
Please answer these questions using only the tools that are provided to you during the exam. This will help you prepare better. During the exam you work on an Ubuntu terminal. The most important CLI tools that you will use are probably kubectl (duh! :D) and the VIM text editor. You might also want to practice your grep skills.  

## Questions
[Core Concepts Pt. 1](#core-concepts-pt.-1)  
[Core Concepts Pt. 2](#core-concepts-pt.-2)   
[Configuration Pt. 1](#configuration-pt.-1)   
[Configuration Pt. 2](#configuration-pt.-2)  
[Multi-container PODs & Observability](#multi-container-pods--observability)  
[POD design](#pod-design)

### Core Concepts Pt. 1
1. Create a YAML file with a pod template. The pod should use the `redis` image and the name of the pod should be `ckad-redis`.    
2. Change the image in the YAML file from `redis` to `busybox` and change the name of the pod to `ckad-busybox`.
3. Create a namespace with the name `nginx`.   
4. Create a pod with the following specs: 
   - name: ckad-nginx
   - image: nginx
   - namespace: nginx
5. Create a pod that has two containers. One container with the name `redis`  that uses the `redis` image. The other container should be called `busybox` which uses the `busybox` image. The name of the pod should be `ckad-practice`. 
  
### Core Concepts Pt. 2
1. Deploy the resources from the file `k8s-templates/02-core-concepts-pt2.yaml`. Wait till all pods are running (this can take a couple of minutes).  
3. In the `einsteinium` namespace your development team deployed a pod named `yellow` for testing purposes. They are happy with how it performs and they want you to turn it into a deployment. The deployment should also be in the `einsteinium` namespace and be called `yellow`. It should have `3` replicas. 
4. In the cluster is a deployment named `red`. Find this deployment and scale the number of replicas to `4`. 
5. In the `krypton` namespace the `blue` deployment has the following image: `image: nginx`. However, not specifying a tag in the image is usually seen as bad practice, because it will always pull the latest image and therefore you have less control. Change the image in the deployment to the `nginx:1.21` image.  
6. Apparently the tag was left out on purpose by your team for whatever reason. Undo the change you made in the previous step. 
7. Whoops, the deployment named `blue` is in the wrong namespace. Move it to the `plutonium` namespace. Make sure to delete the old deployment in the `krypton` namespace. 
8. BONUS QUESTION: Your Product owner wants to have a list of the names of all pods in the `plutonium` namespace. The list must also include the container images and cpu requests used by the container(s) in these pods. Find the information and store it in the file `cpu-request.txt`.   


### Configuration Pt. 1
1. Deploy the resources from the file `k8s-templates/03-configuration-pt1.yaml`. Wait till all pods are running (this can take a couple of minutes).
1. A colleague asks you for your help. He wants to deploy a busybox pod so he can do a couple of connection tests in the cluster. However, the pod keeps exiting straight away. When you run a `docker inspect` on the busybox image you see that the container executes the `sh` command without any arguments. Therefore, the container has nothing to execute and exits immediatly. In order to help your colleague, deploy a pod in the `iron` namespace named `troubleshoot-utils` that has the `busybox` image and executes the command `sh -c "sleep 3600"`. Now the pod should stay up. 
2. You want to test how to mount a file into a pod with a ConfigMap.
   - Create a pod in the `helium` namespace. It should be called `cm-test` and have the `busybox` image. 
   - Mount the file [03-configmap-test.txt](resources/03-configmap-test.txt) in the resources directory as `test.txt` in the pod. Do this by using a ConfigMap. Use the following mountPath: `/resources`. 
   - The pod should execute the following command: `sh -c "while [ true ]; do cat /resources/test.txt; sleep 5; done"`. 
   - You can check whether the pod was deployed successfully by viewing the logs of the pod: `kubectl logs cm-test`. It should output the text from the file.
3. In the `radium` namespace, create a `generic` secret called `postgres`. It should store the following key, value pairs: `USER=postgres-ckad`, `PASSWORD=ckadstudytrack`, `HOSTNAME=postgres`.     
4. Your colleague asks you to find out what text is stored in the secret `secret-text` in the `tin` namespace. Find the secret and save the decoded value of the `not-so-secret-text` key into the file `decoded-text.txt`. 
5. In the `radium` namespace there is a postgres database deployed. You want to practice a bit more with Commands, ConfigMaps and Secrets and therefore you decide to create a pod with a database client and connect to the database. You want to mount a sql script in the pod with a ConfigMap and mount the database credentials with a secret. 
   - Create a pod named `pg-client` in the `radium` namespace that uses the `steindevos/pg-client:1.0` image. It should execute the following command: `sh -c "psql -f scripts/test-script.sql; sleep 3600"`. 
   - As you can see see in the previous step, the container tries to execute a sql script. Mount this script in the pod by using a ConfigMap. Use [resources/03-select-statement.sql](resources/03-select-statement.sql) and mount it as `test-script.sql` in the `/scripts` directory.
   - But wait, how does the pod know how to connect to the database? It uses the following environment variables: `PGUSER`, `PGPASSWORD`, and `PGHOST`. Set these environment variables in the pod. Use the secret `postgres` you created before and make sure to match the values from the secret with the corresponding environment variable. So the value of the key `HOSTNAME` in the secret should be used for the environment variable `PGHOST`
   - After the pod starts succesfullly, output the logs of the pod in the file `pg-client-logs.txt` (use the `kubectl logs` command).   

### Configuration Pt. 2
1. Deploy the resources from the file `k8s-templates/04-configuration-pt2.yaml`. Wait till all pods are running (this can take a couple of minutes).
2. In [this](https://www.youtube.com/watch?v=8vXoMqWgbQQ) video you hear that it is usually bad security practice to run a container as the root user, because it could now potentially have root access on the Docker host. In the `boron` namespace your team just deployed the `blue` deployment and you want to check whether the container runs as root or not.
   - Check the user of the container by executing the `whoami` command in one of the pods of the `blue` deployment. Write the output of the command in the file `blue-user.txt`. 
   - After discussing with the team, you decide that the container in the blue deployment should be run as the user with uid 1001. In order to meet this new requirement, update the `blue` deployment with a securitycontext.
   - After updating the deployment, execute the `whoami` command again in one of the updated pods and save the output in the file `blue-user-updated.txt`.  
3. Team red needs a deployment of `2` nginx pods in the `carbon` namespace. Create and deploy this deployment that should be named `nginx-red`. Use the `nginx:alpine` image. Add the capabilities `"NET_ADMIN"` and `"SYS_TIME"` to the securitycontext. Furthermore, team red has its own service account `team-red-sa` under which the pods should run. 
4. Team green asks for your help. They have deployed the `green` deployment in the `europium` namespace, but the pods don't get up. Figure out why the pods don't get up and fix the deployment accordingly (btw, the cluster administrator wants you to *only* change the deployment and no other resources). 
5. Your colleague asks you to create a redis deployment in the `osmium` namespace. He gives the following requirements: use the `redis:6.2.6` image; it should be named `redis-yellow`; and the number of replicas must be `2`. You advise him to also specify the resource requirements, because otherwise you lack control over the resources used by the pods. He agrees and gives the following resources requirements: a `cpu` request of `200m` and a limit of `200m`; a `memory` request of `256Mi` and a limit of `256Mi`. Create the deployment.    
6. After deploying the `redis-yellow` deployment in the `osmium` your colleague discovers that the limit of the cpu and memory are too low for the redis deployment to perform well. Therefore, he asks you to double both the `cpu` limit as well as the `memory` limit. Update the deployment for your colleague according to the new requirements. 
7. Imagine there is a node with the label `disktype=ssd` and you need to create a pod that can only run on nodes with this specific label. Create the pod template for such a pod and store it in the file `ssd-pod.yaml`. The pod should be named `ssd-pod`, use the `fedora:36` image, and execute the command `sh -c "sleep 3600"`. 


### Multi-container PODs & Observability
1. Deploy the resources from the file `k8s-templates/05-multi-pods-and-observability.yaml`. Wait till all pods are running (this can take a couple of minutes).
2. In the namespace `radon` you have deployed a Postgres database and a pod named `blue` with an sql client that runs an sql script at startup. However, when you view the logging of the `blue` pod you see that the sql script produced an error. This was either because the Postgres database was not up or because the table from the sql script did not exist yet. Copy the logging of the `blue` pod to the file `blue-error.txt`. 
3. In order to make sure that the database is up and that the table exists before the container in the `blue` pod starts, you decide to add an init-container to the `blue` pod. This init-container must run a little script to check for these things.  
Add an init-container to the `blue` pod that uses the image `steindevos/pg-client:1.0`. It should run the command `sh -c "/init-script/init-script.sh"`. Add the database credentials from the `database-cred` secret to the init-container. This is used by the script to connect to the database. Furthermore, the init-script.sh and its accompanying sql script should be mounted from the `init-db` configmap that already exists in the `radon` namespace. Mount the files from this configmap in the `/init-script` directory of the init-container and make sure that the files are executable. You can do this by adding the `defaultMode: 0777` to the configmap in the volume section. It should look something like this: 
```yaml
  volumes:
  - name: init-db
    configMap:
      name: init-db
      defaultMode: 0777
```  
- Restart the pod and - once the pod has started successfully - copy the logging of the `blue` pod to the file `blue-logs.txt`. 
4. You want to practice a bit with creating multi-container pods. Create a multi-container pod named `red` in the `zinc` namespace. The first container should be named `yellow`, use the `busybox:1.35.0` and execute the command `sh -c "for i in $(seq 1 2356); do echo "Hello from the yellow container - $i"; done; sleep 1d"`. The second container should be named `green`, also use the `busybox:1.35.0` image and execute the command `sh -c "for i in $(seq 1 1976); do echo "Hello from the green container - $i"; done; sleep 1d"`. After starting the pod successfully, store the last 200 lines of logging from the `green` container in the file `green-logging.txt`. 
5. Search for the deployment with the label `note: slow-nginx`. There is a pod in the same namespace which is called `check-uptime`. Every second this pod does a http request to one of the pods of the nginx deployment and checks whether the nginx application in the pod was ready to recieve requests or not (by checking the http status code). It logs the number of times the nginx application was up and the number of times nginx was down. Output the last log entry of the `check-uptime` pod in the file `nginx-uptime.txt`. Was there ever any downtime?
6. Update the cpu request for the `nginx` deployment to `150m`.
7. Check the status of the uptime and downtime again by checking the logs of the `check-uptime` pod. Append the last line of logging to the file `nginx-uptime.txt`. Did the number of downtime increase?   
8. Now add a readiness probe to the `nginx` deployment. The readiness probe should initially wait for 10 seconds before it starts and then check every 5 seconds whether the pod is ready to receive requests or not by doing an `httpGet` on port `80` and path `/`. After updating the deployment with the readiness probe append the last line of logging again to the `nginx-uptime.txt` file.  
9. Now update the cpu limit of the `nginx` deployment to `300m`. After the deployment is successfully updated, view the logging of the `check-uptime` pod again and append the last line of logging to the file `nginx-uptime.txt`. Did the downtime increase during the update? 

### POD design
1. Deploy the resources from the file `k8s-templates/06-pod-design.yaml`. Wait till all pods are running (this can take a couple of minutes).
2. In the `calcium` namespace team Red has deployed an Nginx server. They ask you to update the Nginx image for the `nginx` deployment to `nginx:1.21.5`. A requirement is that the image is updated in such a way that the update is stored in the deployment's rollout history. After you've updated the image, copy the rollout history for the `nginx` deployment in the file `nginx-rollout-history.txt`.  
3. Your colleague wants you to add the annotation "warning: do not modify!" to all pods in the `xenon` namespace that have the labels `app: busybox` AND `priority: high` AND a `team` label with a value of either `red` or `blue`. Add the annotation to the specified pods and save a list of only the *names* of these pods in the file `annotated-pods.txt`.  
4. Team green has changed its name to team purple. In the `xenon` namespace, change the `team` label for the pods with the `team: green` label to `team: purple`. Print a list of only the *names* of the pods that were changed in the file `purple-pods.txt`.   
5.  Create a job named `database-insertions` in the `silver` namespace. The job should use the image `steindevos/pg-client:1.0` and execute the command `psql -c "INSERT into ckad (random_text, time) VALUES ('What time was I inserted?', current_timestamp)"`. It should run five times successfully, but not in parallel. Also, add the label `priority: low` to it and import the database credentials as environment variables from the secret `database-cred`. This secret is already created in the `silver` namespace.       
6. Run the job of the previous step again, but this time it should execute all tasks in parallel. Name this job `database-insertions-parallel`.  
7. Turn the job of the previous question into a CronJob. The Cronjob should be named `database-insertions-cj` and should run *once* every minute. Make sure this CronJob is successfully deployed.     
8. The Jobs and CronJob of the previous questions executed a command that inserted data in a Postgres database. We're now going to check whether it succeeded. In the `silver` namespace there is a cronjob named `test-db` that runs a select statement to retrieve all data from the same table that was used in previous questions. Use this CronJob as a template to make a job named `test-db-job` and let it run once. Output the logging of the job into the file `test-db-logs.txt`.

### Services & Networking
1. Deploy the resources from the file `k8s-templates/07-services-and-networking.yaml`. Wait till all pods are running (this can take a couple of minutes).
2. Team Red has deployed an nginx server with a service of type `ClusterIp` in the `lead` namespace. However, it doesn't seem to work properly and they ask you to help them out. Help team Red by troubleshooting the `nginx-red` deployment and service. It should be able to recieve requests successfully. Create a test pod named `troubleshoot-nginx` with the `nginx:1.21.6` image in the `lead` namespace. Execute the command `curl nginx-red:90` from this pod and save the output in the file `nginx-red.txt`.   
3. Team Blue also needs an nginx server with a service. In the `astatine` namespace create the deployment named `nginx-blue` with the `nginx:1.21.6` image. Open container port `80` and add the label `team:blue` to the deployment. Also, mount the file in the ConfigMap `homepage` in the directory `/usr/share/nginx/html`. Expose the deployment through a service named `nginx-blue` of type `ClusterIP`. Use port `81` and target port `80`.  
Deploy a pod with an `nginx` image that successfully executes a curl command to the service. Save the output in the file `nginx-blue.txt`.
4. Team Blue also needs to connect to the `nginx-blue` pod from the `scandium` namespace. Deploy a pod with an `nginx` image in the `scandium` namespace that successfully executes a curl command to the service in the `astatine` namespace and save the output in the file `nginx-blue-other-ns.txt`. 
5. Team Green has deployed a Postgres database in the `terbium` namespace and they want to connect to this database via a service. Create a service named `postgres` that exposes the postgres deployment. The service should be of type `ClusterIP`. The port can be found in the `database-cred` secret as the value of `PGPORT`. The target port should be equal to the container port of the postgres deployment. Test whether the servcice was created successfully by creating a job out of the cronjob `test-db`. Run the job and store the logging of the job in the file `test-db.txt`. 